import pandas as pd
import numpy as np
import pickle
import json
from flask_cors import CORS
from flask import Flask, request

fuel_category_dict = {"gas":0, "diesel":1, "other":2, "hybrid":3, "electric":4}
transmission_category_dict = {"automatic":0, "other":1, "manual":2}
title_status_category_dict = {"clean":0, "rebuilt":1, "salvage":2, "lien":3, "missing":4, "parts only":5}
rfr = pickle.load(open('model_data/rfr.pkl','rb')) # Random Forest Regressor model
manufacturer_label_encoder = pickle.load(open('model_data/manufacturer_label_encoder.pkl','rb'))
model_label_encoder = pickle.load(open('model_data/model_label_encoder.pkl','rb'))
X_scaler = pickle.load(open('model_data/x_scaler.pkl','rb'))
y_scaler = pickle.load(open('model_data/y_scaler.pkl','rb'))

app = Flask(__name__)
cors = CORS(app)

@app.route('/', methods=['POST'])

def predict():

	data = request.get_json(force=True)
	X_values = pd.DataFrame([data])

	# Label encoding of X_values
	X_values[["manufacturer"]] = manufacturer_label_encoder.transform(X_values[["manufacturer"]])
	X_values[["model"]] = model_label_encoder.transform(X_values[["model"]])
	X_values["fuel"] = X_values["fuel"].map(fuel_category_dict)
	X_values["transmission"] = X_values["transmission"].map(transmission_category_dict)
	X_values["title_status"] = X_values["title_status"].map(title_status_category_dict)

	# Scaling of X_values
	X_values = pd.DataFrame(X_scaler.transform(X_values), index=X_values.index, columns=X_values.columns)

	predicted_price = rfr.predict(X_values)
	# De-scaling
	predicted_price = y_scaler.inverse_transform([predicted_price])
	result = {'predicted_price': predicted_price[0][0]}
	result_json = json.dumps(result)

	return(result_json)

if __name__ == '__main__':
	app.run(port = 3002, debug=True)